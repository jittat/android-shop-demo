package com.example.student.shopdemo.data;

import java.util.List;
import java.util.Observable;

/**
 * Created by jittat on 27/4/2560.
 */

public abstract class BookRepository extends Observable {
    public abstract void fetchAllBooks();
    public abstract List<Book> getAllBooks();
}
