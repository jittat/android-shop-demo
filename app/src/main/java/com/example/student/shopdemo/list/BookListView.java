package com.example.student.shopdemo.list;

import com.example.student.shopdemo.data.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jittat on 26/4/2560.
 */

public interface BookListView {
    void setBookList(ArrayList<Book> books);
}
