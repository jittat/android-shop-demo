package com.example.student.shopdemo.list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.student.shopdemo.R;
import com.example.student.shopdemo.data.Book;
import com.example.student.shopdemo.data.BookRepository;
import com.example.student.shopdemo.data.MockBookRepository;
import com.example.student.shopdemo.data.RemoteBookRepository;

import java.util.ArrayList;

public class BookListActivity extends AppCompatActivity implements BookListView {

    BookListPresenter presenter;
    ArrayAdapter<Book> bookArrayAdapter;
    private ListView bookListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        BookRepository repository = RemoteBookRepository.getInstance();

        bookListView = (ListView) findViewById(R.id.listview_books);
        bookArrayAdapter = createAdapter(new ArrayList<Book>());
        bookListView.setAdapter(bookArrayAdapter);

        presenter = new BookListPresenter(repository, this);
        presenter.initialize();
    }

    @Override
    public void setBookList(ArrayList<Book> books) {
        bookArrayAdapter = createAdapter(books);
        bookListView.setAdapter(bookArrayAdapter);
    }

    private ArrayAdapter<Book> createAdapter(ArrayList<Book> books) {
        return new ArrayAdapter<Book>(this,
                android.R.layout.simple_list_item_1,
                books);
    }
}
