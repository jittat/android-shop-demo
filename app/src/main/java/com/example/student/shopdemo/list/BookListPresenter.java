package com.example.student.shopdemo.list;

import com.example.student.shopdemo.data.Book;
import com.example.student.shopdemo.data.BookRepository;
import com.example.student.shopdemo.data.MockBookRepository;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by jittat on 26/4/2560.
 */

public class BookListPresenter implements Observer {
    private BookListView view;
    private BookRepository repository;

    ArrayList<Book> books;

    public BookListPresenter(BookRepository repository, BookListView view) {
        this.repository = repository;
        this.view = view;
    }

    public void initialize() {
        repository.addObserver(this);
        repository.fetchAllBooks();
    }

    @Override
    public void update(Observable obj, Object arg) {
        if(obj == repository) {
            books = new ArrayList<Book>(repository.getAllBooks());
            view.setBookList(books);
        }
    }
}
